# Einführung in Git und Markdown <!-- omit in toc -->

Für die Leistungsbeurteilung (LB2) erstellen Sie ein persönliches Portfolio in Form eines GIT-Repositories. In diesem Block bereiten Sie Ihr persönliches Notebook vor und erlernen die Grundlagen von **Markdown** und **GIT**. 

# Inhalt

Klicke auf die Links, um zu den entsprechenden Inhalte bzw. Aufgaben zu gelangen:

 - Sinn und Zweck der Versionverwaltung mit Luca und Charlie: [Einführung Versionsverwaltung](01_Einführung%20Versionverwaltung.md)
 - [Grundwissen Versionsverwaltung](02_Versionsverwaltung%20Grundwissen.md)
 - Den eigenen Laptop vorbereiten: [Eigene GIT Umgebung](03_Eigene%20GIT%20Umgebung.md)
 - Dokumentieren mit [Markdown](04_Markdown.md)
 - Werde Git Power User mit [Git Branching](05_Git%20Branching.md)
 - Wie schreibt man [Commit messages](06_Commit%20messages.md) ?

# Ziele
 - Sie haben ein GIT-Repository für Ihr persönliches Portfolio erstellt und mit einem Remote-Repository synchronisiert. 
 - Sie kennen die wichtigsten am häufigsten verwendeten Markdown Elemente
 - Sie kennen die wichtigsten GIT-Befehle und ihren Syntax (add, commit, push, pull, status, log).
 - Sie haben einen Quelletext-Editor ausgewählt, auf ihrem persönlichen Notebook eingerichtet und kennen die wichtigsten Bedienelemente. 

# Die wichtigsten Git Befehle
Die wichtigsten und am häufigsten verwendeten Befehle (die man mit der Zeit auswendig kennt):
```bash

# Befehle für den Alltag

git pull                                # pull all new commits from remote repository
# Um sich auf den neuesten Stand des Projektes zu bringen. 
# Das muss man jedesmal machen, bevor man beginnt! 

git add .   oder   git add <file>       # stage one or multiple files
# Damit mache ich alle neuen Dateien für den Git-Transport parat (=stage)


git commit -m "Ein Text für den Commit" # commit all staged files
# Damit schreibe ich das neue Git-Paket an. Die "Message" kann ich später 
# in der Commit-Liste sehen umvielleicht darauf zurückzugreifen.

git push                                # push all new commits to remote repository
# Damit schicke ich die aktuellen Arbeiten (in allen Dateien seit dem letzen pull)
# zum Online-Repository (-Projekt)

# Falls an einem neuen Projekt gearbeitet werden soll (braucht man nur 1x):

git clone <url>                         # clone remote repository in folder


```

# Hilfreiche Links
 - [GIT PURR! Git Commands Explained with Cats!](https://web.archive.org/web/20210918225036/https://girliemac.com/blog/2017/12/26/git-purr/)
 - [Was ist Git](https://michster.de/was-ist-git/)

# Weiterführende Links
 - [Bash cheatsheet](https://devhints.io/bash)
 - [Bash Crash Course](https://zach-gollwitzer.medium.com/the-ultimate-bash-crash-course-cb598141ad03)
 - [Git cheatsheet](https://www.atlassian.com/dam/jcr:e7e22f25-bba2-4ef1-a197-53f46b6df4a5/SWTM-2088_Atlassian-Git-Cheatsheet.pdf)
 - [DevOps](https://de.wikipedia.org/wiki/DevOps)


# Aufgaben

 - [Eigene GIT Umgebung.md/#Eigenen Laptop für die Verwendung von Git vorbereiten](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/10_Git/03_Eigene%20GIT%20Umgebung.md#eigenen-laptop-f%C3%BCr-die-verwendung-von-git-vorbereiten)
