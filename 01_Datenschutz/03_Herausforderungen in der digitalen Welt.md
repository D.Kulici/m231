# Herausforderungen in der Digitalen Welt
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Gruppenarbeit |
| Aufgabenstellung  | Recherche, Diskussion |
| Zeitbudget  |  1 bis 2 Lektionen |
| Ziel | Sensibilisierung zu Themen rund um unsere digitale Welt |

![Symbolbild](media/Symbolbild_Film.PNG)

1969 waren Tablets erst eine Fanatasie von Science Fiction Authoren. Im Film [*2001 A SPACE ODYSSEY*](https://www.slashgear.com/tablet-computer-spotted-in-1968-kubrick-classic-cited-in-samsung-case-23173802/) schauen die zwei Weltraum Exploreres während dem Essen in ihre Tabelts. Inzwischzeit träumen die Menschen nicht nur von Tablets: iPads, Android Tablets sind inzwischen weltweit verfügbar. Die laufend auf den Markt kommenden Technologien bringen mehr immer Möglichkeiten. Dabei brigen sie auf laufend neue Herausforderungen. 

## Aufgabenstellung 
 - Die Klasse wird von der LP in Gruppen aufgeteilt. 
 - Jede Gruppe wählt eines der nachfolgenden Themen aus. Schreiben Sie die Namen der Gruppenmitglieder und das Thema auf die Wandtafel. <br>Es gilt: *First come, first served*
 - Schauen Sie sich die Quellen an, recherchieren Sie selbstständig weiter und diskutieren Sie in der Gruppe über ihr Thema
 - Überlegen Sie in der Gruppe: 
   - Was sind die spannenden Herausforderungen für die Zukunft?
   - Was gibt es für kontroverse Fragen mit der sich die Gesellschaft in Zukunft beschäftigen wird?
   - Inwiefern spielt das Thema Datenschutz in Zukunft eine Rolle?
 - Halten Sie die wichtigen Punkte auf einem Poster (Digital oder auf Papier) fest.
 - Anschliessend präsentiert jede Gruppe die Resultate ihrer Diskussion und stellt der Klasse vor.
 - Jede Gruppe führt mit der Klasse ein kurze Diskussionrunde mit den zuvor überlegten Fragen. 

## Beispiel für kontroverse Fragen

**Thematik: Selbstfahrende Autos**

 - Wer haftet, wenn ein selbstfahrendes Auto einen Unfall macht?

Szenario: Auto rast auf einen Fussgänger:

(1) Erste Möglichkeit: Ausweichen und in einen Baum fahren: Fussgänger gerettet, Insassen tot. 

(2) Zweite Möglichkeit: Nicht Ausweichen: Fussgänger tot, Insassen unverletzt

Soll das Auto die Insassen schützen oder den Fussgänger?

## Themen

### Risiken von "künstlicher Intelligenz"
![https://xkcd.com/2265/](media/tax_ai.png)

*Quelle: https://xkcd.com/2265/*

 - Vortrag * Maschinelles Lernen & warum es gruseliger ist als man denkt (Jonas Betzendahl – Science Slam)*:  https://www.youtube.com/watch?v=BZszcoE2QDA
 - Elon Musk: ‘Mark my words — A.I. is far more dangerous than nukes’: https://www.cnbc.com/2018/03/13/elon-musk-at-sxsw-a-i-is-more-dangerous-than-nuclear-weapons.html
 - Beitrag im "Bayerischen Ärtzeblatt": https://www.bayerisches-aerzteblatt.de/fileadmin/aerzteblatt/ausgaben/2019/09/einzelpdf/442_443.pdf
 - Google sperrt Konton wegen Kinderfotos: https://www.morgenpost.de/vermischtes/article236227241/google-konto-sperrung-intime-kinderfotos-usa.html
 - Viele Weitere Beiträge: https://www.google.com/search?q=gefahren+machine+learning


### Deep Fakes
![https://xkcd.com/2650](media/deepfakes_2x.png)

*Quelle: https://xkcd.com/2650*

Tom Cruise Deep Fakes gingen Viral. Unzählige *Shorts* tauchten auf Youtube, TikTok, Instagram, die überzeugend echte Videos mit dem Schauspieler zeigen. 
 - The Person Behind the Viral Tom Cruise Deepfake | Super Users - https://www.youtube.com/watch?v=p7-B8S734T4
 - Deep Fakes / Quelle: https://www.netzwoche.ch/news/2022-03-18/deepfake-video-gaukelt-kapitulierenden-ukrainischen-praesidenten-vor

### Online-Communitys

![https://xkcd.com/802/](media/online_communities_2.png)

*Quelle: https://xkcd.com/802/*

Das Internet erlaubt es, dass Menschen sich viel einfacher in Subkulturen radikalisieren können. Welcher Herausforderungen gehen davon aus?

 - Verschwörungstheorien / Flat Earthener / Quelle: https://de.wikipedia.org/wiki/Flat_Earth_Society
 - Wikipedia-Artikel zum Thema Incel: https://de.wikipedia.org/wiki/Incel
 - Hunting Down Incel Extremists | Investigators: https://www.youtube.com/watch?v=9p7dRa9-7as

