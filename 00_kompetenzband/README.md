# Kompetenzmatrix - Modul 231

## Handlungsziele und typische Handlungssituationen
**1: "Kategorisiert Daten aufgrund ihres Schutzbedarfs"**<br/>
**5: "Zeigt Konsequenzen von Fehlern im Datenschutz und bei der Datensicherheit auf."**

Paul Muster, 1. Lehrjahr Lernender, macht im Ausgang Photos und posted diese mit coolen Sprüchen auf sozialen Plattformen. Doch halt, was ist erlaubt? Wo sind die Grenzen?
Paul Muster muss sich mit schützenswerten Daten, dem **Umgang mit Bildern, dem Recht am eigenen Bild, dem Urheberrecht, Sexting, Cyber-Mobbing, Memes, Videos**, Aufnahmen mit Drohnen auseinandersetzen.
Er muss sich anhand von Weisungen, Verordnungen, Gesetzen (zBsp Datenschutzgesetz) informieren und als Informatiker korrekt mit schützenswerten Daten umgehen und andere im Umgang schulen können.
Hilfsmittel: zBsp Unterlagen des Datenschutzbeauftragten Kanton Zürich

**2: "Überprüft und verbessert Datensicherheit der eigenen Infrastruktur"**<br/>
**3: "Setzt verschiedene Möglichkeiten der Datenspeicherung ein"**<br/>
**6: Wählt Software für die Einhaltung von Datenschutz und Datensicherheit aufgrund der Lizenz Modelle aus"**

Paul Muster, 1. Lehrjahr Lernender, muss seine **Ablage der Daten** aus Schule, Betrieb und Privat **organisieren**. Er muss eine Ablagestruktur entwerfen und umsetzen. Dabei muss er mit **unterschiedlichen Speicherorten** (lokal, Cloud, OneDrive, Googledrive.. ) umgehen können. 
Für besonders sensitive Daten setzt Paul Muster ein **Verschlüsselung von Dokumenten, Verzeichnissen oder Partitionen** ein (zBsp Veracrypt, bitlocker..). (**<== Satz kaputt!**)
Für die Datensicherung entwirft Paul Muster ein **Konzept für ein Backup** und setzt dieses um.

**4 "Überprüft eingesetzte Anwendungen auf Einhalt der Datenschutzgesetze"**<br/>
**6 "Wählt Software für die Einhaltung von Datenschutz und Datensicherheit aufgrund der Lizenz Modelle aus"**

Paul Muster verwendet im Betrieb, der Schule und der Freizeit unterschiedliche Programme und Plattformen. Er **konfiguriert Programme in Hinsicht auf den Datenschutz und der Datensicherheit**. (ZBsp auf der Basis der Checklisten des Datenschutzbeauftragten Kanton Zürich: Browser, Facebook ...)
Er kennt die Unterschiede verschiedener Lizenz Modelle und kann eine **Software auf Grund der Anforderungen an das Lizenz Modell begründet auswählen**.

**1 "Kategorisiert Daten aufgrund ihres Schutzbedarfs"**<br/>
**4 "Überprüft eingesetzte Anwendungen auf Einhalt der Datenschutzgesetze"**

Paul Muster erhält den Auftrag die **Webseite** des lokalen Fussballclubs **kritisch nach schützenswerten Daten und den Umgang mit dem Datenschutz zu analysieren**. Er unterscheidet dazu zwischen allgemeine und personenbezogene Daten. Er überprüft zBsp den Zugriffsschutz auf das Mitgliederverzeichnis (Rechte, Rollen, User) . Er macht **begründete Verbesserungsvorschläge**. Er gibt Auskunft wie zBsp zur Passwortverwaltung, "gute" Passwörter, Passwortmanagern.



## Matrix

| Kompetenzband: | HZ | Grundlagen | Fortgeschritten | Erweitert |
| --------------------- | --------------------------- | ------------------ | ------------------ | ------------------ |
| Schutzwürdigkeit von Daten im Internet | 1, 2 | **A1G**: Kann schützenswerte Daten erkennen. | **A1F**: Wendet in seiner Umgebung den Schutz der Daten konsequent an und befolgt Regeln. | **A1E**: Kann unterschiedliche Regelungen (zBps DSG, DSGVO, Schulordnung) vergleichen und kritisch hinterfragen. |
| Datensicherheit der eigenen Infrastruktur | 2, 5 | **B1G**: Kann den Grundsatz Daten und Applikation zu trennen erläutern | **B1F**: Wendet ein vorgegebenes Ablagekonzept an, um die eigenen Daten zu organisieren und zu speichern. | **B1E**: Entwirft ein eigenes Ablagekonzept für seine Daten und setzt dieses um. |
|                   |       3, 6       | **B2G**: Kann den Unterschied zwischen lokaler und cloudbasierter Ablage erläutern. | **B2F**: Richtet und setzt lokale und cloudbasierte Ablagen ein. | **B2E**: Vergleicht  verschiedene cloudbasierte  Ablagen. |
| | 3, 6 | **B3G**: Kann den Zweck, aber auch die Grenzen eines Backups erläutern. | **B3F**: Plant und macht regelmässig ein Backup seiner Daten. | **B3E**: Setzt ein Backupkonzept ein und überprüft es regelmässig auf Restorefähigkeit. |
| Datenverschlüsselung | 2, 3, 6 | **C1G**: Kann das Prinzip der Verschlüsselung von Daten erläutern und kennt die Unterschiede von client- und serverseitiger Verschlüsselung. | **C1F**: Richtet und setzt Verschlüsselungen für Daten ein. | **C1E**: Hinterfragt eingesetzte Verschlüsselungen, macht Verbesserungsvorschläge, zeigt Möglichkeiten zur Wiederherstellung auf. |
| Lizenzmodelle | 6 | **D1G**: Kann den Zweck von Lizenzen erläutern. ("4 Arten der Lizenzmodelle") | **D1F**: Kann für eine Anforderung eine Software gezielt und auf der Basis der Lizenz auswählen und einsetzen. | **D1E**: Kann ein bestehendes Lizenzmodell auf rechtsmässigen Einsatz überprüfen. |
| Passwörter | 2, 5 | **E1G**: Kann die Prinzipen der Passwortverwaltung und "gute" Passwörter erläutern. | **E1F**: Kann einen Passwort Manager einsetzen. | **E1E**: Kennt und setzt erweiterte Möglichkeiten der Authentifizierung ein. Kennt Vor- und Nachteile von Passwortmanagern. |
| Anwendungen auf Datenschutzkonformität überprüfen | 4 | **F1G**: Kann die Problematik bei unsicher konfigurierten Anwendungen erläutern. | **F1F**: Kann bestehende Anwendungen überprüfen. | **F1E**: Kann Anwendungen und Profile anhand von Checklisten sicherer machen. |
|  | 4 | **F2G**: Kennt die Bedeutung der AGBs und Datenschutzrichtlinien von Anbietern. | **F2F**: Kann die Datenschutzrichtlinie bezüglich Datenspeicherung überprüfen. |  |

## Kompetenzstufen

### Grundlagen | Stufe 1 

Diese Stufe ist als Einstieg ins Thema gedacht. Der Fokus liegt hier auf dem Verstehen von Begriffen und Zusammenhängen. 

*Als Richtungshinweis: Wer alle Kompetenzen in dieser Stufe erfüllt, hat die Noten 3.0.*

### Fortgeschritten | Stufe 2 

Diese Stufe definiert den Pflichtstoff, den alle Lernenden am Ende des Moduls möglichst beherrschen sollen.  

*Als Richtungshinweis: Wer alle Kompetenzen in dieser Stufe erfüllt, hat die Noten 4.5*

### Erweitert | Stufe 3 

Diese Lerninhalte für Lernende gedacht, die schneller vorankommen und einen zusätzlichen Lernanreiz erhalten sollen.  

*Als Richtungshinweis: Wer alle Kompetenzen in dieser Stufe erfüllt, hat die Noten 6*
